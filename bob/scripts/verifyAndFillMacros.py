# -----------------------------------------------------------------------------
# Jython - CSStudio
# -----------------------------------------------------------------------------
# Verify and fill MACROS accordingly
# -----------------------------------------------------------------------------
# ESS ERIC - ICS HWI group
# -----------------------------------------------------------------------------
# WP12 - douglas.bezerra.beniz@esss.se
# -----------------------------------------------------------------------------

# Read information for the devices from an XML file
from org.csstudio.display.builder.runtime.script import PVUtil, ScriptUtil
from org.csstudio.display.builder.model import WidgetFactory
from org.csstudio.display.builder.model.properties import ActionInfos, OpenDisplayActionInfo

import xml.etree.ElementTree as ElementTree
import os, sys, time

from time import sleep
from array import array
from jarray import zeros

# -----------------------------------------------------------------------------
# Definition of constants used
# -----------------------------------------------------------------------------

# -----------------------------------------------------------------------------
# class objects
# -----------------------------------------------------------------------------
logger = ScriptUtil.getLogger()

# -----------------------------------------------------------------------------
# procedures
# -----------------------------------------------------------------------------
def verificationProcedure():
    # Locate XML file relative to the display file
    display_file = ScriptUtil.workspacePathToSysPath(widget.getDisplayModel().getUserData("_input_file"))
    directory    = os.path.dirname(display_file)
    file         = directory + "/scripts/macros.xml"
    display      = widget.getDisplayModel()

    embedded = display.runtimeChildren().getChildByName('displayAzuraP61L')
    # Parse XML
    # Actual content of the XML file (MACROS) would of course depend on what's needed by this device.
    xml = ElementTree.parse(file).getroot()
    for macro_list in xml.iter("macros"):
        for macro in macro_list:
            # Debug purposes...
            # logger.info("macro.tag: %s; macro.text: %s" % (str(macro.tag), str(macro.text)))
            embedded.getPropertyValue("macros").add(macro.tag, macro.text)
    # Setting the main display to load...
    embedded.setPropertyValue("file", "azuraP61L.bob")

# -----------------------------------------------------------------------------
# calling the main procedure
# -----------------------------------------------------------------------------
#sleep(0.2)              # this was necessary because more than one procedure were being started, probably due to the period of scan of CSStudio thread
verificationProcedure()
